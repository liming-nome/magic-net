﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 职位服务
/// </summary>
[ApiDescriptionSettings(Name = "Pos", Order = 100, Tag = "职位服务")]
public class SysPosController : IDynamicApiController
{
    private readonly ISysPosService _service;
    public SysPosController(ISysPosService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页获取职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysPos/page")]
    public async Task<PageList<SysPos>> PageList([FromQuery] QueryPosPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取职位列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysPos/list")]
    public async Task<List<SysPos>> List([FromQuery] QueryPosPageInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 增加职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysPos/add")]
    public async Task Add(AddPosInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysPos/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysPos/edit")]
    public async Task UpdatePos(EditPosInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysPos/detail")]
    public async Task<SysPos> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }
}

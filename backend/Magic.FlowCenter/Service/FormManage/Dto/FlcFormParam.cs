﻿using Magic.Core;
using System.ComponentModel.DataAnnotations;

namespace Magic.FlowCenter.Service;

public class QueryFlcFormPageInput : PageParamBase
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long OrgId { get; set; }
}

public class AddFlcFormInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "表单名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 机构Id
    /// </summary>
    public long OrgId { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    public FormType FrmType { get; set; }

    public string WebId { get; set; }
}

public class EditFlcFormInput : AddFlcFormInput
{
    public long Id { get; set; }
}

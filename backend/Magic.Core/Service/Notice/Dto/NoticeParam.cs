﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 通知公告参数
/// </summary>
public class QueryNoticePageInput : PageParamBase
{
    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public virtual NoticeType Type { get; set; }

}

public class AddNoticeInput
{
    /// <summary>
    /// 标题
    /// </summary>
    [Required(ErrorMessage = "标题不能为空")]
    public string Title { get; set; }

    /// <summary>
    /// 内容
    /// </summary>
    [Required(ErrorMessage = "内容不能为空")]
    public string Content { get; set; }

    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    [Required(ErrorMessage = "类型不能为空")]
    public NoticeType Type { get; set; }

    /// <summary>
    /// 状态（字典 0草稿 1发布 2撤回 3删除）
    /// </summary>
    [Required(ErrorMessage = "状态不能为空")]
    public NoticeStatus Status { get; set; }

    /// <summary>
    /// 通知到的人
    /// </summary>
    [Required(ErrorMessage = "通知到的人不能为空")]
    public List<long> NoticeUserIdList { get; set; }
}

public class EditNoticeInput: AddNoticeInput
{ 
    public long Id { get; set; }
}

public class ChangeStatusNoticeInput : Core.PrimaryKeyParam
{
    /// <summary>
    /// 状态（字典 0草稿 1发布 2撤回 3删除）
    /// </summary>
    [Required(ErrorMessage = "状态不能为空")]
    public NoticeStatus Status { get; set; }
}


#region 输出参数
/// <summary>
/// 通知公告接收参数
/// </summary>
public class NoticeReceiveOutput : NoticeBase
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 阅读状态（字典 0未读 1已读）
    /// </summary>
    public int ReadStatus { get; set; }

    /// <summary>
    /// 阅读时间
    /// </summary>
    public DateTime ReadTime { get; set; }
}

/// <summary>
/// 系统通知公告详情参数
/// </summary>
public class NoticeDetailOutput : NoticeBase
{
    /// <summary>
    /// 通知到的用户Id集合
    /// </summary>
    public List<string> NoticeUserIdList { get; set; }

    /// <summary>
    /// 通知到的用户阅读信息集合
    /// </summary>
    public List<NoticeUserRead> NoticeUserReadInfoList { get; set; }
}


public class NoticeUserRead
{
    /// <summary>
    /// 用户Id
    /// </summary>
    public long UserId { get; set; }

    /// <summary>
    /// 用户名称
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// 状态（字典 0未读 1已读）
    /// </summary>
    public NoticeUserStatus ReadStatus { get; set; }

    /// <summary>
    /// 阅读时间
    /// </summary>
    public DateTime ReadTime { get; set; }
}
#endregion
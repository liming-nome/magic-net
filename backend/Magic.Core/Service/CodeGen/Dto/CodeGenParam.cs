﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

#region 输入参数
/// <summary>
/// 分页获取生成器主表参数
/// </summary>
public class QueryCodeGenPageInput : PageParamBase
{
    /// <summary>
    /// 数据库表名
    /// </summary>
    public virtual string TableName { get; set; }
}

public class AddCodeGenInput
{
    /// <summary>
    /// 数据库表名
    /// </summary>
    [Required(ErrorMessage = "数据库表名不能为空")]
    public string TableName { get; set; }

    /// <summary>
    /// 业务名（业务代码包名称）
    /// </summary>
    [Required(ErrorMessage = "业务名不能为空")]
    public string BusName { get; set; }

    /// <summary>
    /// 命名空间
    /// </summary>
    [Required(ErrorMessage = "命名空间不能为空")]
    public string NameSpace { get; set; }

    /// <summary>
    /// 作者姓名
    /// </summary>
    [Required(ErrorMessage = "作者姓名不能为空")]
    public string AuthorName { get; set; }


    /// <summary>
    /// 生成方式
    /// </summary>
    [Required(ErrorMessage = "生成方式不能为空")]
    public string GenerateType { get; set; }


    /// <summary>
    /// 菜单应用分类（应用编码）
    /// </summary>
    [Required(ErrorMessage = "菜单应用分类不能为空")]
    public string MenuApplication { get; set; }

    /// <summary>
    /// 菜单父级
    /// </summary>
    [Required(ErrorMessage = "菜单父级不能为空")]
    public long MenuPid { get; set; }
}

public class EditCodeGenInput : AddCodeGenInput
{
    public long Id { get; set; }
}


/// <summary>
/// 获取配置列表参数
/// </summary>
public class QueryCodeGenConfigInput
{
    /// <summary>
    /// 代码生成主表ID
    /// </summary>
    public long CodeGenId { get; set; }
}


#endregion

#region 输出参数
/// <summary>
/// 数据库表列
/// </summary>
public class TableColumnOutput
{
    /// <summary>
    /// 字段名
    /// </summary>
    public string ColumnName { get; set; }

    /// <summary>
    /// 数据库中类型
    /// </summary>
    public string DataType { get; set; }

    /// <summary>
    /// .NET字段类型
    /// </summary>
    public string NetType { get; set; }

    /// <summary>
    /// 字段描述
    /// </summary>
    public string ColumnComment { get; set; }

    /// <summary>
    /// 主外键
    /// </summary>
    public string ColumnKey { get; set; }
}

/// <summary>
/// 数据库表列表参数
/// </summary>
public class TableOutput
{
    /// <summary>
    /// 表名（字母形式的）
    /// </summary>
    public string TableName { get; set; }

    /// <summary>
    /// 实体名称
    /// </summary>
    public string EntityName { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public string CreateTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    public string UpdateTime { get; set; }

    /// <summary>
    /// 表名称描述（注释）（功能名）
    /// </summary>
    public string TableComment { get; set; }
}
#endregion
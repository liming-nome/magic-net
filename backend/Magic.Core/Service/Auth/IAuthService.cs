﻿using Furion.DependencyInjection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface IAuthService: ITransient
{
    Task<ClickWordCaptchaResult> GetCaptcha();
    Task<bool> GetCaptchaOpen();
    Task<LoginOutput> GetLoginUserAsync();
    Task<string> LoginAsync(LoginInput input);
    Task LogoutAsync();
    Task<ClickWordCaptchaResult> VerificationCode(ClickWordCaptchaInput input);

    Task<string> SimulationLoginAsync(LoginInput input);
}
